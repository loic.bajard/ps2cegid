<?php

define("LOG_FILE", __DIR__."/logs/importCustomers.log");


error_reporting("E_ALL");
ini_set('display_error', 1);
require_once 'SPDO.php';
require_once 'CsvTools.php';
require_once 'Logger.php';

$logger = new Logger();
$SPDO = SPDO::getInstance();

$customersCsv = new CsvTools(__DIR__."/import/Client.txt",6);
$customersMapCsv = new CsvTools(__DIR__."/Customers.map.csv",3);
$customersMap = $customersMapCsv->import(1);
$customers = $customersCsv->import();
// First backup customers actual table
$SPDO->backUpTable("ps_customer");

// Truncate temp table before importing
$query = $SPDO->prepare("TRUNCATE TABLE ps_tmp_customer");
$query->execute();

$tmpFields = array();
$prestaFields = array();
foreach ($customersMap as $row){
 $tmpFields[] = $row[2];
 $prestaFields[] = $row[0];
}

// Insert csv into temp table
$logger->log("Insert csv into temp table");
try{
$sql = "INSERT INTO ps_tmp_customer(".implode(",",$tmpFields).") VALUES (?,?,?,?,?,?)";
$req = $SPDO->prepare($sql);

foreach ($customers as $row) {
	$req->execute($row);
}

} catch(PDOException $ex){
	var_dump($ex->getMessage());
}

// Query tmp database to import new customers wich have a valid email
$logger->log("Select tmp table to import new customers");
$sql = "SELECT tmp.".implode(",tmp.",$tmpFields)." FROM ps_tmp_customer tmp LEFT JOIN ps_customer ps ON ps.email=tmp.email WHERE tmp.email REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]\.[a-zA-Z]{2,4}$' AND tmp.prestashop_customer_id=0 AND ps.email IS NULL";
$req = $SPDO->prepare($sql);
$req->execute();

$res = $req->fetchAll();

// Insert them into actual customers table
$logger->log("Insert customers into actual table");
$sql = "INSERT INTO ps_customer(".implode(",",$prestaFields).",secure_key,passwd,active,date_add,date_upd,id_default_group) VALUES (?,?,?,?,?,?,?,?,1,NOW(),NOW(),11)";
$req = $SPDO->prepare($sql);
$insertedIds = array();
foreach ($res as $key => $row) {
	$values = array();
	// mapping values
	foreach ($tmpFields as $key) {
		$values[] = $row[$key];
	}
	// Add random values
	$values[] = md5(uniqid(rand(),true)); //secure_key
	$values[] = md5(uniqid(rand(),true)); //passwd
	$req->execute($values);
	$logger->log("Insertion nouveau client #".$SPDO->lastInsertId(). "(code CEGID : ".$values['cegid_code'].")");
	$insertedIds[] = $SPDO->lastInsertId();
}


// Insert them into group table
if(!empty($insertedIds)){
    $logger->log("Insert new customers into temp group");
    $sql = "INSERT INTO ps_customer_group(id_customer,id_group) VALUES(?,11)";
    $req = $SPDO->prepare($sql);

    foreach ($insertedIds as $id) {
            $req->execute(array($id));
            $logger->log("Insertion dans groupe d'import, client #.".$id);
    }
}

//$sql = "SELECT tmp.".implode(",tmp.",$tmpFields)." FROM ps_tmp_customer tmp LEFT JOIN ps_customer ps ON ps.email=tmp.email WHERE ps.email IS NOT NULL";

// Fetch customers to update
$logger->log("Fetch customers to update");
$sql = "SELECT tmp.".implode(",tmp.",$tmpFields)." FROM ps_tmp_customer tmp WHERE tmp.prestashop_customer_id != 0";
$req = $SPDO->prepare($sql);
$req->execute();
$res = $req->fetchAll();

// Prepare update query
$logger->log("Update customers");
$sql = "UPDATE ps_customer SET ";
foreach ($prestaFields as $key) {
	$sql .= $key . " = ? , ";
}
$sql = substr($sql, 0, -2);
$sql .= " WHERE email = ?";

$req = $SPDO->prepare($sql);

// Loop through results to update customers table
foreach ($res as $row) {
    $values = array();
    // Mapping values
    foreach ($prestaFields as $key => $prestaField) {
                $values[] = $row[$tmpFields[$key]];
    }
    $values[] = $row["email"];
    $req->execute($values);
    $logger->log("MAJ client #".$row['email']);
}