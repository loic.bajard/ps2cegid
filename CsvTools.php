<?php

/**
 * Provides tools to manage csv files from/to cegid like import,export etc
 */
class CsvTools {
	private $nbCols;
	private $content;
	private $logger;
	
	public function __construct($fileName,$nbCols) {
		$this->logger = new Logger();
		if(!is_readable($fileName)){
			$this->logger²->error($fileName. "not readable");
			return false;
		}
		$this->fileName = $fileName;
		$this->nbCols = $nbCols;
	}
	/**
	 * Import the csv content into an array
	 */
	public function import($lineToIgnore=0) {
		$row = 1;
		if (($fHandle = fopen($this->fileName,"r"))) {
			$tmpArray = array();
			while ($data = fgetcsv($fHandle,1500,";")){
				if($row > $lineToIgnore){
					if(count($data) > 0){
						if($this->nbCols != count($data)) $this->logger->warn("Invalid columns number at line ".$row);
						$tmpRow = array();
						for ($i=0; $i < $this->nbCols; $i++) {
							$tmpRow[] = $data[$i];
						}
						$tmpArray[] = $tmpRow;
					}
				}
				$row++;
			}
			$this->setContent($tmpArray);
			return $this->getContent();
		}
	}

	public function getContent(){
		return $this->content;
	}
	public function setContent($content) {
		$this->content = $content;
	}
}
